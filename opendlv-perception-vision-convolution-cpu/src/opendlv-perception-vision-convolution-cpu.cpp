/*
 * Copyright (C) 2020 Ola Benderius
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <memory>
#include <mutex>

#include <X11/Xlib.h>

#include "cluon-complete.hpp"

void convolution(uint8_t *img, uint8_t *dst, uint32_t w, uint32_t h, uint32_t i,
    uint32_t j, uint32_t c)
{
  if (i == 0 || i == (w-1) || j == 0 || j == (h-1)) {
    dst[j * w + i] = 0;
  } else {
    uint32_t b{j * w * 4 + i * 4 + c};
    int32_t v = img[b - 4 * w] + img[b - 4] + img[b + 4] + img[b + 4 * w] 
      - 4 * img[b];
    v = std::min(v, 255);
    v = std::max(v, 0);
    dst[j * w + i] = static_cast<uint8_t>(v);
  }
}

void channelToImage(uint8_t *image, uint8_t *channel, uint32_t width,
    uint32_t height)
{
  for (uint32_t j{0}; j < height; j++) {
    for (uint32_t i{0}; i < width; i++) {
      for (uint32_t c{0}; c < 3; c++) {
        image[j * width * 4 + i * 4 + c] = channel[j * width + i];
      }
    }
  }
}

int32_t main(int32_t argc, char **argv) {
  auto commandlineArguments = cluon::getCommandlineArguments(argc, argv);
  if ((0 == commandlineArguments.count("name")) ||
      (0 == commandlineArguments.count("width")) ||
      (0 == commandlineArguments.count("height"))) {
    std::cerr << argv[0] << " attaches to a shared memory area containing an "
      << "ARGB image to find edges using convolution." << std::endl;
    std::cerr << "Usage:   " << argv[0] << " <OPTION>" << std::endl;
    std::cerr << "  --name:   name of the shared memory area to attach"
      << std::endl;
    std::cerr << "  --width:  width of the frame" << std::endl;
    std::cerr << "  --height: height of the frame" << std::endl;
    std::cerr << "Example: " << argv[0] << " --name=img.argb --width=640 "
      << "--height=480 --verbose" << std::endl;
    return 0;
  }

  std::string const name{commandlineArguments["name"]};
  uint32_t const width{static_cast<uint32_t>(
      std::stoi(commandlineArguments["width"]))};
  uint32_t const height{static_cast<uint32_t>(
      std::stoi(commandlineArguments["height"]))};
  bool const verbose{commandlineArguments.count("verbose") != 0};

  uint8_t image[width * height * 4];
  uint8_t conv[width * height];

  Display* display{nullptr};
  Visual* visual{nullptr};
  Window window{0};
  XImage* ximage{nullptr};

  if (verbose) {
    display = XOpenDisplay(nullptr);
    visual = DefaultVisual(display, 0);
    window = XCreateSimpleWindow(display, RootWindow(display, 0), 0, 0, width,
        height, 1, 0, 0);
    ximage = XCreateImage(display, visual, 24, ZPixmap, 0, 
        reinterpret_cast<char *>(image), width, height, 8, 0);
    XMapWindow(display, window);
  }

  std::unique_ptr<cluon::SharedMemory> sharedMemory{
    new cluon::SharedMemory{name}};
  if (sharedMemory && sharedMemory->valid()) {
    std::clog << argv[0] << ": Attached to shared memory '"
      << sharedMemory->name() << " (" << sharedMemory->size() << " bytes)."
      << std::endl;

    uint32_t const n = 300;
    std::array<double, n> times;
    double timeSum{0};

    for (uint32_t k{0}; k < n; k++) {
      sharedMemory->wait();
      sharedMemory->lock();
      
      cluon::data::TimeStamp start = cluon::time::now();
      
      memcpy(image, sharedMemory->data(), height * width * 4);
      sharedMemory->unlock();


      for (uint32_t j{0}; j < height; j++) {
        for (uint32_t i{0}; i < width; i++) {
          convolution(image, conv, width, height, i, j, 0);
        }
      }

      double time = (cluon::time::toMicroseconds(cluon::time::now())
          - cluon::time::toMicroseconds(start)) / 1000.0;
      std::cout << "Time per frame: " << time << " ms (k=" << +k << " of " 
        << n << ")" << std::endl;

      times[k] = time;
      timeSum += time;


      if (verbose) {
        channelToImage(image, conv, width, height);
        XPutImage(display, window, DefaultGC(display, 0), ximage, 0, 0, 0, 0,
            width, height);
      }
    }

    double mean{timeSum / n};
    double variance{0.0};
    {
      double tmp{0};
      for (uint32_t k{0}; k < n; k++) {
        tmp += (times[k] - mean) * (times[k] - mean);
      }
      variance = tmp / (n - 1);
    }
    std::cout << "Time per frame: mean " << mean << " ms, std " 
      << std::sqrt(variance) << std::endl;
  }
  return 0;
}
